<?php

/**
 * Fired during plugin activation
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Stats
 * @subpackage Gc_Stats/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gc_Stats
 * @subpackage Gc_Stats/includes
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gc_Stats_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
