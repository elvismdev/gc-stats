<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Stats
 * @subpackage Gc_Stats/public/partials
 */
?>
<div id="gc-stats">
	<div class="container">
		<div class="row">
			<div class="cu_members">
				<a href="http://cardoneuniversity.com">
					<span><?php echo number_format_i18n( $gcs_data['gcs_cu'] ); ?></span> <?php echo __( 'Members Served', 'gc-stats' ); ?>
				</a>
			</div>
			<div class="fb_likes">
				<a href="https://www.facebook.com/grantcardonefan">
					<span><?php echo number_format_i18n( $gcs_data['gcs_fb'] ); ?></span> <?php echo __( 'Page Likes', 'gc-stats' ); ?>
				</a>
			</div>
			<div class="tweet_follow">
				<a href="https://twitter.com/grantcardone">
					<span><?php echo number_format_i18n( $gcs_data['gcs_twitter'] ); ?></span> <?php echo __( 'Followers', 'gc-stats' ); ?>
				</a>
			</div>
			<div class="instagram">
				<a href="https://www.instagram.com/grantcardone/">
					<span><?php echo number_format_i18n( $gcs_data['gcs_instagram'] ); ?></span> <?php echo __( 'Followers', 'gc-stats' ); ?>
				</a>
			</div>
			<div class="youtube">
				<a href="https://www.youtube.com/user/GrantCardone">
					<span><?php echo number_format_i18n( $gcs_data['gcs_yt'] ); ?></span> <?php echo __( 'Subscribers', 'gc-stats' ); ?>
				</a>
			</div>
			<div class="gplus">
				<a href="https://plus.google.com/106268128704132883440">
					<span><?php echo number_format_i18n( $gcs_data['gcs_gplus'] ); ?></span> <?php echo __( 'Followers', 'gc-stats' ); ?>
				</a>
			</div>
			<div class="linkedin">
				<a href="https://www.linkedin.com/in/grantcardone">
					<span><?php echo number_format_i18n( $gcs_data['gcs_linkedin'] ); ?></span> <?php echo __( 'Followers', 'gc-stats' ); ?>
				</a>
			</div>
			<div class="periscope">
				<a href="https://www.periscope.tv/grantcardone">
					<span><?php echo number_format_i18n( $gcs_data['gcs_periscope'] ); ?></span> <?php echo __( 'Followers', 'gc-stats' ); ?>
				</a>
			</div>
		</div>
	</div>
</div>