<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Stats
 * @subpackage Gc_Stats/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gc_Stats
 * @subpackage Gc_Stats/admin
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gc_Stats_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gc_Stats_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gc_Stats_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gc-stats-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gc_Stats_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gc_Stats_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gc-stats-admin.js', array( 'jquery' ), $this->version, false );

	}


	public function gcs_add_admin_menu(  ) { 

		add_menu_page( 
			'GC Stats', 
			'GC Stats', 
			'manage_options', 
			'gc_stats', 
			array( $this, 'gcs_options_page' ),
			'dashicons-admin-site' 
			);

	}


	public function gcs_settings_init(  ) {

		register_setting( 
			$this->plugin_name . '-options', 
			$this->plugin_name . '-options'
			);

		add_settings_section(
			$this->plugin_name .'-section', 
			__( 'Define GC numbers down here', 'gc-stats' ), 
			'gcs_settings_section_callback', 
			$this->plugin_name
			);

		add_settings_field( 
			'gcs_cu', 
			__( 'CU Members', 'gc-stats' ), 
			array( $this, 'gcs_text_field_render' ), 
			$this->plugin_name, 
			$this->plugin_name .'-section',
			array(
				'id' => 'gcs_cu'
				)
			);

		add_settings_field( 
			'gcs_fb', 
			__( 'Facebook', 'gc-stats' ), 
			array( $this, 'gcs_text_field_render' ), 
			$this->plugin_name, 
			$this->plugin_name .'-section',
			array(
				'id' => 'gcs_fb'
				) 
			);

		add_settings_field( 
			'gcs_twitter', 
			__( 'Twitter', 'gc-stats' ), 
			array( $this, 'gcs_text_field_render' ), 
			$this->plugin_name, 
			$this->plugin_name .'-section',
			array(
				'id' => 'gcs_twitter'
				)  
			);

		add_settings_field( 
			'gcs_instagram', 
			__( 'Instagram', 'gc-stats' ), 
			array( $this, 'gcs_text_field_render' ), 
			$this->plugin_name, 
			$this->plugin_name .'-section',
			array(
				'id' => 'gcs_instagram'
				) 
			);

		add_settings_field( 
			'gcs_yt', 
			__( 'Youtube', 'gc-stats' ), 
			array( $this, 'gcs_text_field_render' ), 
			$this->plugin_name, 
			$this->plugin_name .'-section',
			array(
				'id' => 'gcs_yt'
				) 
			);

		add_settings_field( 
			'gcs_gplus', 
			__( 'Google +', 'gc-stats' ), 
			array( $this, 'gcs_text_field_render' ), 
			$this->plugin_name, 
			$this->plugin_name .'-section',
			array(
				'id' => 'gcs_gplus'
				) 
			);

		add_settings_field( 
			'gcs_linkedin', 
			__( 'LinkedIn', 'gc-stats' ), 
			array( $this, 'gcs_text_field_render' ),
			$this->plugin_name, 
			$this->plugin_name .'-section',
			array(
				'id' => 'gcs_linkedin'
				)
			);

		add_settings_field( 
			'gcs_periscope', 
			__( 'Periscope', 'gc-stats' ), 
			array( $this, 'gcs_text_field_render' ),
			$this->plugin_name, 
			$this->plugin_name .'-section',
			array(
				'id' => 'gcs_periscope'
				) 
			);


	}


	public function gcs_text_field_render( $args ) { 

		$options = get_option( $this->plugin_name . '-options' );
		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-field-text-admin-display.php' );

	}


	public function gcs_settings_section_callback(  ) { 

		echo __( 'This section description', 'gc-stats' );

	}


	public function gcs_options_page(  ) { 

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-admin-display.php' );

	}

}
