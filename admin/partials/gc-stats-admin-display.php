<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Stats
 * @subpackage Gc_Stats/admin/partials
 */
?>

<div class="wrap">
	<form action='options.php' method='post'>

		<h2>GC Stats</h2>

		<?php
		settings_fields( $this->plugin_name . '-options' );
		do_settings_sections( $this->plugin_name );
		submit_button( 'Save Stats' );
		?>

	</form>
</div>
