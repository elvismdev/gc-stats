<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Stats
 * @subpackage Gc_Stats/admin/partials
 */
?>

<input type='text' name='<?php echo $this->plugin_name . '-options[' . $args['id'] . ']'; ?>' value='<?php echo $options[$args['id']]; ?>'>
